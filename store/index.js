export const state = () => ({
    log: false
  })
  
  export const getters = {
    getLog(state) {
      return state.log
    }
  }

  export const mutations = {
    toggleLog(state) {
      state.log=!state.log
    }
  }